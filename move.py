#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.wizard import Wizard
from trytond.pyson import PYSONEncoder
from trytond.pool import Pool


class Move(ModelSQL, ModelView):
    _name = 'account.move'

    def create_move_reverse(self, move_id):
        line_obj = Pool().get('account.move.line')
        tax_line_obj = Pool().get('account.tax.line')
        new_id = self.copy(move_id)
        move = self.browse(new_id)
        for line in move.lines:
            vals = {
                'debit': line.debit * -1,
                'credit': line.credit * -1,
                }
            line_obj.write(line.id, vals)
            for tax_line in line.tax_lines:
                vals = {'amount': tax_line.amount * -1}
                tax_line_obj.write(tax_line.id, vals)
        return new_id

Move()


class MoveReverse(Wizard):
    'Move Reverse'
    _name = 'account.move.reverse'

    states = {
        'init': {
            'result': {
                'type': 'action',
                'action': '_action_create_move_reverse',
                'state': 'end',
            },
        },
    }

    def _action_create_move_reverse(self, data):
        pool = Pool()
        move_obj = pool.get('account.move')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')

        new_ids = []
        for move_id in data['ids']:
            new_id = move_obj.create_move_reverse(move_id)
            new_ids.append(new_id)

        model_data_ids = model_data_obj.search([
                ('fs_id', '=', 'act_move_form'),
                ('module', '=', 'account'),
                ('inherit', '=', False),
                ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        if len(new_ids)==1:
            res['res_id'] = new_ids[0]
            res['views'].reverse()
        else:
            res['pyson_domain'] = PYSONEncoder().encode([
                ('id', 'in', new_ids),
                ])
        return res

MoveReverse()
