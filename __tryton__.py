# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Move Reverse',
    'name_de_DE': 'Buchhaltung Stornobuchung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Reverse moves in Accounting
    - Provides the possibility to create reversing entries in accounting
     (Cancelation)
''',
    'description_de_DE': '''Stornobuchungen für die Buchhaltung
    - Ermöglicht die Eingabe von Umkehrbuchungen in der Buchhaltung (Storno)
''',
    'depends': [
        'account'
    ],
    'xml': [
        'move.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
